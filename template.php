<?php

/**
 * Preprocess variables for html.tpl.php
 *
 * @see system_elements()
 * @see html.tpl.php
 */
function yeti_base_preprocess_html(&$variables) {

}

/**
 * Implements hook_library().
 */
function yeti_base_library() {

}

/**
 * Override the submitted variable.
 */
function yeti_base_preprocess_node(&$variables) {

}
