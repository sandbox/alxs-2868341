var gulp = require('gulp');
var $    = require('gulp-load-plugins')();
var gutil = require('gulp-util');
var clear = require('clear');
var inquirer = require('inquirer');
var fs = require('fs');

var sassPaths = [
  'scss'
];

// Global install task
gulp.task('install', ['install:foundation']);

// Foundation install parent task
gulp.task('install:foundation', ['install:foundation:settings', 'install:foundation:globals', 'compile:foundation:sass']);

gulp.task('install:foundation:settings', function() {
  // Check if Foundation settings partial already exists
  fs.stat('scss/_settings.scss', function(err, stat) {
    if(err == null) {
      console.log('Settings file exists.');
    } else {
      console.log('Settings file does not exist.');
      inquirer.prompt([{
        type: 'list',
        name: 'install_foundation_settings',
        message: 'Would you like to install the default Foundation settings partial?',
        choices: [
          'Yes',
          'No'
        ]
      }])
      .then(function(res) {
        if (res['install_foundation_settings'] == 'Yes') {
          gulp.src('bower_components/foundation-sites/scss/settings/_settings.scss')
            .pipe(gulp.dest('scss'));
          console.log('Foundation settings file has now been installed');
        } else {
          return;
        }
      });
    }
  });
});

gulp.task('install:foundation:globals', function() {
  // Check if Foundation global partial already exists
  fs.stat('scss/_global.scss', function(err, stat) {
    if(err == null) {
      console.log('Global variables file exists.');
    } else {
      console.log('Global variables file does not exist.');
      inquirer.prompt([{
        type: 'list',
        name: 'install_foundation_globals',
        message: 'Would you like to install the default Foundation global variables partial?',
        choices: [
          'Yes',
          'No'
        ]
      }])
      .then(function(res) {
        if (res['install_foundation_globals'] == 'Yes') {
          gulp.src('bower_components/foundation-sites/scss/_global.scss')
            .pipe(gulp.dest('scss'));
          console.log('Foundation variables file has now been installed');
        } else {
          return;
        }
      });
    }
  });
});

gulp.task('install:foundation:files', function(cb) {
  inquirer.prompt([{
    type: 'list',
    name: 'install_foundation_sass_files',
    message: 'Would you like to install the Foundation SCSS files?',
    choices: [
      'Yes',
      'No'
    ]
  }])
  .then(function(res) {
    if (res['install_foundation_sass_files'] == 'Yes') {
      gulp.src('bower_components/foundation-sites/_vendor/**/*.scss')
        .pipe($.copy('scss/_vendor', {prefix: 3}));
      gulp.src('bower_components/foundation-sites/scss/**/*.scss')
        .pipe($.ignore(['foundation.scss', '**/_settings.scss']))
        .pipe($.copy('scss/foundation', {prefix: 4}));
      gulp.src('bower_components/motion-ui/src/**/*.scss')
        .pipe($.copy('scss/motion-ui', {prefix: 3}));
      gutil.log('Foundation SCSS files & dependencies have now been installed.');
    } else {
      return;
    }
  });
  cb();
});

gulp.task('compile:foundation:sass', ['install:foundation:files'], function() {
  gulp.src('scss/*.scss')
    .pipe($.sass({
      includePaths: sassPaths
    })
    .on('error', $.sass.logError))
    .pipe($.autoprefixer({
      browsers: ['last 2 versions', 'ie >= 9']
    }))
    .pipe(gulp.dest('css'));

  return;
});

gulp.task('sass', function() {
  return gulp.src('scss/*.scss')
    .pipe($.sass({
      includePaths: sassPaths
    })
    .on('error', $.sass.logError))
    .pipe($.autoprefixer({
      browsers: ['last 2 versions', 'ie >= 9']
    }))
    .pipe(gulp.dest('css'));
});

gulp.task('default', ['sass'], function() {

  gutil.log('watching for .scss file changes in /scss.');
  gulp.watch(['scss/**/*.scss'], ['sass']);
});
